﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace FakesCreate
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void folderSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fb = new FolderBrowserDialog();
            if (fb.ShowDialog() == DialogResult.OK)
            {
                textFolder.Text = fb.SelectedPath;
                textFolder.Show();
            }
        }

        private void run_Click(object sender, EventArgs e)
        {
            string path = textFolder.Text;
            int i;
            for (i = path.Length - 1; i >= 0; i--)
                if (char.IsLetterOrDigit(path, i) && path[i] != '\\')
                    break;
            if (i != path.Length - 1)
                path = path.Substring(0, i + 1);
            if (!Directory.Exists(path))
            {
                Console.WriteLine("path " + path + " does not exist");
                Console.ReadLine();
                return;
            }
            string[] vFolders;
            vFolders = Directory.GetDirectories(path);
            List<string> vFails = new List<string>();
            foreach (string sFldr in vFolders)
            {
                if (String.Compare(sFldr, path + "\\FakesByTypes", comparisonType: StringComparison.OrdinalIgnoreCase) == 0 ||
                    String.Compare(sFldr, path + "\\OriginalByTypes", comparisonType: StringComparison.OrdinalIgnoreCase) == 0)
                    continue;
                string sFail = sFldr + ",";
                int lst = sFldr.LastIndexOf("\\");
                string fn = sFldr;
                if (lst >= 0)
                    fn = fn.Substring(lst + 1);
                string json = sFldr + "\\json_response.json";
                string card = sFldr + "\\normalised_image.png";
                string info = sFldr + "\\info.json";
                if (!File.Exists(json) || !File.Exists(card) || !File.Exists(info))
                {
                    vFails.Add(sFldr + ", Reason: Missing file");
                    continue;
                }
                string typeDir = JsonRead(path, json);
                if (typeDir == null)
                {
                    sFail += "Reason: Failed reading json file or Id 000...";
                    vFails.Add(sFail);
                    continue;
                }
                Text = "Arrange by types and extract face for " + fn;
                Rectangle rect = GetCropRectangle(info);
                if (rect.Width < 50 || rect.Height < 50)
                {
                    sFail += "Reason: Can't find photo in info.json";
                    vFails.Add(sFail);
                    continue;
                }
                if (!Directory.Exists(typeDir))
                {
                    Directory.CreateDirectory(typeDir + "\\cards");
                    Directory.CreateDirectory(typeDir + "\\faces");
                }

                //string jsonTarget = typeDir + "\\json_response.json";
                //if (!File.Exists(jsonTarget))
                //    File.Copy(json, jsonTarget);
                rect.X = Math.Max(0, rect.Left);
                rect.Y = Math.Max(0, rect.Top);
                Bitmap img = (Bitmap)Image.FromFile(card);
                if (rect.X + rect.Width >= img.Width)
                    rect.Width = img.Width - rect.X - 1;
                if (rect.Y + rect.Height >= img.Height)
                    rect.Height = img.Height - rect.Y - 1;
                lst = card.LastIndexOf(".");
                string ext = card.Substring(lst);
                CropAndSave(img, rect, typeDir + "\\faces\\" + fn + ".jpg");
                File.Copy(card, typeDir + "\\cards\\" + fn + ext, true);
                string details = rect.X.ToString() + "," + rect.Y.ToString() + "," + rect.Width.ToString() + "," + rect.Height.ToString();
                File.WriteAllText(typeDir + "\\faces\\" + fn + ".csv", details);
            }
            File.WriteAllLines(path + "\\zzFailedFolders.csv", vFails.ToArray());
            //create fakes. For each image in the pos folder of every type, randomly choose face image of different type and the frame of the current image
            string sFakePath = path + "\\FakesByTypes";
            if (!Directory.Exists(sFakePath))
                Directory.CreateDirectory(sFakePath);
            vFolders = Directory.GetDirectories(path + "\\OriginalByTypes");
            List<string> vLog = new List<string>();
            vLog.Add("Original, Face from, Type of document, Original width, Original height");

            int iFldrsNum = vFolders.Length;
            foreach (string sFldr in vFolders)
            {
                int pureInd = sFldr.LastIndexOf("\\");
                string sPure;
                if (pureInd <= 0)
                    sPure = sFldr;
                else
                    sPure = sFldr.Substring(pureInd + 1);
                sPure = "\\" + sPure + "\\";
                string[] vFiles = Directory.GetFiles(sFldr + "\\cards");
                foreach (string simg in vFiles)
                {
                    int lst = simg.LastIndexOf('\\');
                    int ext = simg.LastIndexOf(".");
                    if (lst < 0 || ext < 0)
                        continue;
                    string fn = simg.Substring(lst + 1, ext - lst - 1);
                    Text = "Create fake for " + fn;
                    string sRect = File.ReadAllText(sFldr + "\\faces\\" + fn + ".csv");
                    int[] vPar = new int[4];
                    for (int pr = 0; pr < 4; pr++)
                    {
                        int cm = sRect.IndexOf(',');
                        if (cm >= 0)
                        {
                            vPar[pr] = Int32.Parse(sRect.Substring(0, cm));
                            sRect = sRect.Substring(cm + 1);
                        }
                        else
                            vPar[pr] = Int32.Parse(sRect);
                    }
                    string face = SelectFace(vFolders, sFldr);
                    if (!Directory.Exists(sFakePath + sPure))
                        Directory.CreateDirectory(sFakePath + sPure);
                    FakeCreate(sFakePath + sPure + fn + ".jpg", simg, face, vPar, ref vLog, sPure);
                }
            }
            File.WriteAllLines(path + "\\log.csv", vLog.ToArray());
            Text = "DONE!";
            System.Media.SystemSounds.Asterisk.Play();

        }

        private string JsonRead(string path, string json)
        {
            string[] jsonLines = File.ReadAllLines(json);
            bool bCont = false;
            string type = null;
            foreach (string ln in jsonLines)
            {
                int ind = ln.IndexOf("ClassificationDetails\":");
                if (ind < 0)
                    continue;
                string rest = ln.Substring(ind);
                ind = rest.IndexOf("Id\"");
                if (ind < 0)
                    continue;
                string id = rest.Substring(ind + 6, 36);
                if (String.Compare(id, "00000000-0000-0000-0000-000000000000", true) == 0)
                {
                    bCont = true;
                    break;
                }
                type = id;
                break;
            }
            if (bCont)
                return null;
            string typeDir = path + "\\OriginalByTypes\\" + type;
            return typeDir;
        }

        private Rectangle GetCropRectangle(string info)
        {
            string[] infoLines = File.ReadAllLines(info);
            bool bFound = false;
            int[] vVals = new int[4];
            string[] vKeys = { "height", "width", "x", "y" };
            foreach (string ln in infoLines)
            {
                int pht = ln.IndexOf("Photo\":");
                if (pht < 0)
                    continue;
                string sphoto = ln.Substring(pht);
                int brckRight = sphoto.IndexOf('}');
                if (brckRight < 0 || brckRight > 60)
                    continue;
                sphoto = ln.Substring(pht, brckRight);
                pht = sphoto.IndexOf('{');
                sphoto = sphoto.Substring(pht + 1);
                int coma = -1, semi = -1;
                for (int i = 0; i < 4; i++)
                {
                    bool bComaFix = false;
                    coma = sphoto.IndexOf(',');
                    semi = sphoto.IndexOf(':');
                    if (semi < 0 || coma < 0 && i < 3)
                        break;
                    if (coma < 0)
                    {
                        coma = sphoto.Length;
                        bComaFix = true;
                    }
                    string key = sphoto.Substring(0, semi);
                    string sVal = sphoto.Substring(semi + 1, coma - semi - 1);
                    int val = Int32.Parse(sVal);
                    int n;
                    for (n = 0; n < 4; n++)
                        if (key.IndexOf(vKeys[n]) >= 0)
                        {
                            vVals[n] = val;
                            break;
                        }
                    if (n >= 4)
                    {
                        semi = -1;
                        break;
                    }
                    if (!bComaFix)
                        sphoto = sphoto.Substring(coma + 1);
                }
                if (semi < 0)
                    break;
                bFound = true;
            }
            if (!bFound)
                return new Rectangle();
            return new Rectangle(vVals[2], vVals[3], vVals[1], vVals[0]);
        }

        private void CropAndSave(Bitmap img, Rectangle rect, string fn)
        {
            Bitmap target = new Bitmap(rect.Width, rect.Height);

            using (Graphics g = Graphics.FromImage(target))
            {
                g.DrawImage(img, new Rectangle(0, 0, target.Width, target.Height),
                                 rect,
                                 GraphicsUnit.Pixel);
            }
            ImageSave(target, fn);
            target.Dispose();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void ImageSave(Bitmap target, string fn)
        {
            ImageCodecInfo myImageCodecInfo;
            System.Drawing.Imaging.Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;
            myImageCodecInfo = GetEncoderInfo("image/jpeg");
            myEncoder = System.Drawing.Imaging.Encoder.Quality;
            myEncoderParameters = new EncoderParameters(1);
            myEncoderParameter = new EncoderParameter(myEncoder, 85L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            target.Save(fn, myImageCodecInfo, myEncoderParameters);
        }

        private ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        private string SelectFace(string[] vFolders, string cur)
        {
            int num = vFolders.Length;
            bool bFound = false;
            Random rand = new Random();
            string ret = null;
            do
            {
                int choose = rand.Next(0, num);
                if (string.Compare(vFolders[choose], cur, true) == 0)
                    continue;
                string[] vFiles = Directory.GetFiles(vFolders[choose] + "\\faces");
                if (vFiles.Length < 2)
                    continue;
                choose = rand.Next(0, vFiles.Length / 2);
                ret = vFiles[2 * choose + 1];
                bFound = true;
            } while (!bFound);
            return ret;
        }

        private void FakeCreate(string fake, string orig, string face, int[] vPar, ref List<string> vLog, string sPure)
        {
            Bitmap img = (Bitmap)Image.FromFile(orig);
            Rectangle rect = new Rectangle(vPar[0], vPar[1], vPar[2], vPar[3]);
            Bitmap toResize = (Bitmap)Image.FromFile(face), resized = null;
            if (toResize.Width != rect.Width || toResize.Height != rect.Height)
                resized = ResizeImage(toResize, rect.Width, rect.Height);
            else
                resized = (Bitmap)toResize.Clone();
            Graphics g = Graphics.FromImage(img);
            g.DrawImage(resized, rect.X, rect.Y);
            ImageSave(img, fake);
            string sw = img.Width.ToString();
            string sh = img.Height.ToString();
            img.Dispose();
            toResize.Dispose();
            resized.Dispose();
            g.Dispose();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            int lst = orig.LastIndexOf("\\");
            string sOrig, sFace;
            if (lst > 0)
                sOrig = orig.Substring(lst + 1);
            else
                sOrig = orig;
            int ext = sOrig.LastIndexOf(".");
            if (ext >= 0)
                sOrig = sOrig.Substring(0, ext);
            lst = face.LastIndexOf("\\");
            if (lst > 0)
                sFace = face.Substring(lst + 1);
            else
                sFace = face;
            ext = sFace.LastIndexOf(".");
            if (ext >= 0)
                sFace = sFace.Substring(0, ext);
            vLog.Add(sOrig + "," + sFace + "," + sPure.Substring(1, sPure.Length - 2) + "," + sw + "," + sh);
        }

        private static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

    }
}
