﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace RefModify
{
    class Program
    {
        static void Main(string[] args)
        {
            string srefF = ConfigurationManager.AppSettings["ref"];
            string stoAdd = ConfigurationManager.AppSettings["toAddBefore"];
            string[] vRef = File.ReadAllLines(srefF);
            List<string> vRes = new List<string>();
            foreach (string s in vRef)
            {
                int ind = s.IndexOf(":");
                if (ind < 0)
                    continue;
                int dot = s.Substring(0, ind - 1).LastIndexOf('.');
                string res = stoAdd + s.Substring(0, dot+1) + "jpg " + s.Substring(ind);
                vRes.Add(res);
                Console.WriteLine(s);
            }
            Console.WriteLine("DONE");
            File.WriteAllLines(srefF, vRes.ToArray());
        }
    }
}
