﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace FoldersTree2FileList
{
  class Program
  {
    static void TreeWalker(string origPath, string path, ref Queue<string> vList)
    {
      string[] vFolders, vFiles;
      if (!Directory.Exists(path))
      {
        Console.WriteLine("path " + path + " does not exist");
        Console.ReadLine();
        return;
      }
      vFolders = Directory.GetDirectories(path);
      vFiles = Directory.GetFiles(path);
      foreach (string f in vFiles)
      {
        int ind = f.LastIndexOf('.');
        if (ind < 0)
          continue;
        string suf = f.Substring(ind + 1).ToLower();
        if (/*suf == "jpg" || */suf == "ejpg" /*|| suf == "jpeg" || suf == "bmp" || suf == "ebmp"*/)
          vList.Enqueue(f.Substring(origPath.Length));
      }
      foreach (string f in vFolders)
      {
        int ind = f.IndexOf("uncropped");
        if (ind > 0)
          continue;
        TreeWalker(origPath, f + "\\", ref vList);
      }
    }
   static void Main(string[] args)
    {
      string path = ConfigurationManager.AppSettings["InitialPath"];
      Queue<string> vList = new Queue<string>();
      int i;
      for (i = path.Length - 1; i >= 0; i--)
        if (!Char.IsWhiteSpace(path[i]))
          break;
      if (i < 0)
      {
        Console.WriteLine("Wrong path");
        return;
      }
      if (i < path.Length - 1)
        path = path.Substring(0, i+1);
      if (path[i] != '\\')
        path += "\\";
      string outFile = path + ConfigurationManager.AppSettings["ListFile"];
      TreeWalker(path, path, ref vList);
      File.WriteAllLines(outFile, vList.ToArray());
    }
  }
}
