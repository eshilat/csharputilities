﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace GlareAnalyze
{
    class Program
    {
        static int min2(int a, int b) { if (a < b) return a; return b; }

        public static void Empty(System.IO.DirectoryInfo directory)
        {
            foreach (System.IO.FileInfo file in directory.GetFiles()) file.Delete();
            foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
        }

        static void Main(string[] args)
        {
            string refPath = ConfigurationManager.AppSettings["RefFile"];
            string[] reff = File.ReadAllLines(refPath);
            string[] res = File.ReadAllLines(ConfigurationManager.AppSettings["ResFile"]);
            if (res.Length != reff.Length)
            {
                Console.WriteLine("different length for result and reference files");
                return;
            }
            int pathInd = refPath.LastIndexOf('\\');
            string baseRefPath = refPath.Substring(0, pathInd + 1);
            string[] prnt = new string[reff.Length + 4];
            int filesCnt = 0, glareFilesCnt = 0, noglareFilesCnt = 0, glareMis = 0, noglareMis = 0, ignoreCnt = 0;
            System.IO.DirectoryInfo directoryGlr = new System.IO.DirectoryInfo(baseRefPath + "GlareMisses\\");
            System.IO.DirectoryInfo directoryNoGlr = new System.IO.DirectoryInfo(baseRefPath + "NoGlareMisses\\");
            Empty(directoryGlr);
            Empty(directoryNoGlr);
            for (int i = 0; i < min2(reff.Length, res.Length); i++)
            {
                int indRef = reff[i].LastIndexOf(':');
                int eqRef = reff[i].LastIndexOf('=');
                int indRes = res[i].LastIndexOf(':');
                int eqRes = res[i].LastIndexOf('=');
                if (indRef < 0 || indRes < 0 || eqRef < 0 || eqRes < 0)
                {
                    Console.WriteLine("Wrong file structure: " + reff[i] + " " + res[i]);
                    break;
                }
                string fnRef = reff[i].Substring(0, indRef);
                string fnRes = res[i].Substring(0, indRes);
                if (fnRef.ToLower() != fnRes.ToLower())
                {
                    Console.WriteLine("Files do not match: " + fnRef + "\t" + fnRes);
                    break;
                }
                string sGradeRef = reff[i].Substring(eqRef + 1);
                string sGradeRes = res[i].Substring(eqRes + 1);
                int brac = sGradeRef.IndexOf('[');
                if (brac > 0)
                    sGradeRef = sGradeRef.Substring(0, brac);
                float fGradeRef = float.Parse(sGradeRef);
                float fGradeRes = float.Parse(sGradeRes);
                filesCnt++;
                if (fGradeRef > 0.49 && fGradeRef < 0.51)
                    ignoreCnt++;
                if (fGradeRef < 0.49)
                {
                    //{
                    //    string from = baseRefPath + fnRef;
                    //    string to = baseRefPath + "Glares\\" + Path.GetFileName(from);
                    //    File.Copy(from, to);
                    //}
                    glareFilesCnt++;
                    if (fGradeRes > 0.5)
                    {
                        glareMis++;
                        string from = baseRefPath + fnRef;
                        string to = baseRefPath + "GlareMisses\\" + Path.GetFileName(from);
                        try
                        {
                            File.Copy(from, to);
                        }catch(Exception ex)
                        {
                            Console.WriteLine(from + " failed to copy to GlareMisses. Error: "+ex.ToString() );
                        }
                    }
                }
                if (fGradeRef > 0.51 )
                {
                    //{
                    //    string from = baseRefPath + fnRef;
                    //    string to = baseRefPath + "GlaresWithout\\" + Path.GetFileName(from);
                    //    File.Copy(from, to);
                    //}
                    noglareFilesCnt++;
                    if (fGradeRes < 0.5)
                    {
                        noglareMis++;
                        string from = baseRefPath + fnRef;
                        string to = baseRefPath + "noGlareMisses\\" + Path.GetFileName(from);
                        try
                        {
                            File.Copy(from, to);
                        }catch(Exception ex)
                        {
                            Console.WriteLine(from + " failed to copt to NoGlareMisses. Error: " + ex.ToString() );
                        }
                    }
                }
                prnt[i + 4] = fnRef + ". ref grade = " + sGradeRef + ", res grade = " + sGradeRes;
            }
            prnt[0] = "Total files: " + filesCnt + ". Ignored: " + ignoreCnt;
            prnt[1] = "Glare files: " + glareFilesCnt + " Missed glare files: " + glareMis + ". Glares grade = " + (1 - (float)(glareMis) / glareFilesCnt);
            prnt[2] = "No glare files: " + noglareFilesCnt + " Missed: " + noglareMis + ". No glares grade = " + (1 - (float)noglareMis / noglareFilesCnt);
            prnt[3] = "Final grade = " + (1 - (float)(glareMis + noglareMis) / (float)(glareFilesCnt + noglareFilesCnt));
            File.WriteAllLines(ConfigurationManager.AppSettings["OutFile"], prnt);
        }
    }
}
