﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Collections.Generic;


namespace FromRef2Folder
{
    class Program
    {
        static void Main(string[] args)
        {
            string refPath = ConfigurationManager.AppSettings["RefFile"];
            string resPath = ConfigurationManager.AppSettings["Output"];
            string[] refr = File.ReadAllLines(refPath);
            List<string> vList = new List<string>();
            int pathInd = refPath.LastIndexOf('\\');
            string baseRefPath = refPath.Substring(0, pathInd + 1);
            string refFileName = refPath.Substring(pathInd + 1);

            for (int i = 0; i < refr.Length; i++)
            {
                int indRef = refr[i].LastIndexOf(':');
                if (indRef < 0)
                {
                    Console.WriteLine("Wrong structure of reference file");
                    break;
                }
                string fnRef = refr[i].Substring(0, indRef);
                string RefInfo = refr[i].Substring(indRef);
                string from = baseRefPath + fnRef;
                int slash = fnRef.LastIndexOf('\\');
                if (slash >= 0)
                    fnRef = fnRef.Substring(slash + 1);
                vList.Add(i.ToString("D5") + "_" + fnRef + RefInfo);
                string to = resPath + i.ToString("D5")+"_" + fnRef;
                File.Copy(from, to, true);
                Console.WriteLine(to);
            }
            Console.WriteLine("Done");
            File.WriteAllLines(resPath + refFileName, vList.ToArray());
            System.Media.SystemSounds.Asterisk.Play();
        }
    }
}
