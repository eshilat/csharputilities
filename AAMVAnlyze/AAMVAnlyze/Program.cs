﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;   //right click References and choose "add reference" and under "Framework" choose Configuration manager to make ConfigurationManager recognized.

namespace AAMVAnlyze
{
  class CBasicBlock
  {
    public CBasicBlock(int len, int type, string str)
    {
      m_iLen = len;
      m_iType = type;
      m_sList = str;
    }
    public override bool Equals(Object obj)
    {
      if (obj == null || GetType() != obj.GetType())
        return false;
      CBasicBlock bb = (CBasicBlock)obj;
      return (m_iLen == bb.m_iLen && m_iType == bb.m_iType);
    }
    public override int GetHashCode()
    {
      return (m_iLen ^ m_iType) ^ m_sList.Length;
    }
    public void Update(CBasicBlock bb)
    {
      string sOther = bb.m_sList;
      for (int i = 0; i < sOther.Length; i++)
        if (m_sList.IndexOf(sOther[i]) < 0)
          m_sList += sOther[i];
    }
    //======================
    public int m_iLen;
    public int m_iType; //0 - letter, 1 - digit, 2 - symbol
    public string m_sList;  //contains the different values
  }

  class CFieldFormat
  {
    public CFieldFormat()
    {
      m_sStart = "";
      m_sEnd = "";
      m_vBlocks = new List<CBasicBlock>();
      m_iCnt = 0;
    }
    public CFieldFormat(string strt, string end)
    {
      m_sStart = strt;
      m_sEnd = end;
      m_vBlocks = new List<CBasicBlock>();
      m_iCnt = 0;
    }
    public int TypeSet(char c)
    {
      int typ = 2;
      if (c >= '0' && c <= '9')
        typ = 1;
      else if ((c >= 'A' && c <= 'Z') || c == ' ')
        typ = 0;
      return typ;
    }
    public void String2Field(string line)
    {
      CBasicBlock bb;
      if (line.Length == 0)
        return;
      int typ = TypeSet(line[0]);
      int len = 1;
      string str = line.Substring(0, 1);
      for (int i = 1; i < line.Length; i++)
      {
        int curTyp = TypeSet(line[i]);
        if (curTyp == typ)
        {
          len++;
          if (str.IndexOf(line[i]) < 0)
            str += line[i];
        }else
        {
          bb = new CBasicBlock(len, typ, str);
          m_vBlocks.Add(bb);
          m_iCnt++;
          len = 1;
          typ = curTyp;
          str = line.Substring(i, 1);
        }
      }
      bb = new CBasicBlock(len, typ, str);
      m_vBlocks.Add(bb);
      m_iCnt++;
    }
    public void Update(CFieldFormat ff)
    {
      for (int i = 0; i < m_iCnt; i++)
        m_vBlocks[i].Update(ff.m_vBlocks[i]);
    }
    public override bool Equals(Object obj)
    {
      if (obj == null || GetType() != obj.GetType())
        return false;
      CFieldFormat ff = (CFieldFormat)obj;
      if (ff.m_iCnt != m_iCnt || ff.m_sEnd != m_sEnd || m_sStart != ff.m_sStart)
        return false;
      for (int i = 0; i < m_iCnt; i++)
        if (!(m_vBlocks[i] == ff.m_vBlocks[i]))
          return false;
      return true;
    }
    public override int GetHashCode()
    {
      return m_iCnt ^ m_sEnd.Length ^ m_sStart.Length ^ m_vBlocks.Count;
    }
    private string m_sStart, m_sEnd;
    List<CBasicBlock> m_vBlocks;
    int m_iCnt;
  }
  class CAnalyze
  {
    public CAnalyze()
    {
      m_iCounter = 0;
      m_vKeys = new Dictionary<string, List<CFieldFormat>>();
    }

    public bool IsInList(string key)
    {
      string[] vKeys = new string[] {"DAA", "DAG", "DAI", "DAJ", "DAK", "DAQ", "DAR", "DAS", "DAT", "DAT", "DBA", "DBB", "DBC", "DBD", "DAU", "DAW", "DAY", "DAZ", "DBK", "PAA", "PAB", "PAC", "PAD", "PAE",
                      "PAF", "DAB", "DAC", "DAD", "DAE", "DAF", "DAH", "DAL", "DAM", "DAN", "DAO", "DAP", "DAV", "DAX", "DBE", "DBF", "DBG", "DBH", "DBI", "DBJ", "DBL", "DBM", "DBN", "DBO", "DBP", "DBQ", "DBR", "DBS"};
      foreach (string ky in vKeys)
        if (ky == key)
          return true;
      return false;
    }
    public bool ReadLine(string start, string line)
    {
      CFieldFormat ff = new CFieldFormat(start, "");
      string key = line.Substring(0, 3);
      int stringCrop = 3;
      if (!IsInList(key))
      {
        if (key[0] != 'Z')
          return false;
        key = line.Substring(0, 2);
        stringCrop = 2;
      }
      line = line.Substring(stringCrop);
      ff.String2Field(line);
      List<CFieldFormat> tmp = null;
      if (m_vKeys.TryGetValue(key, out tmp))
      {
        bool bExist = false;
        foreach (CFieldFormat fld in m_vKeys[key])
        {
          if (fld == ff)
          {
            bExist = true;
            fld.Update(ff);
            break;
          }
        }
        if (!bExist)
        {
          m_vKeys[key].Add(ff);
        }
      }
      else
      {
        tmp = new List<CFieldFormat>();
        tmp.Add(ff);
        m_vKeys[key] = tmp;
      }
      return true;
    }
    public int Counter
    {
      get
      {
        return m_iCounter;
      }
      set
      {
        m_iCounter = value;
      }
    }

    public void SetDL(int offset, int size, int offsetGap)
    {
      m_vDLOffset.Add(offset);
      m_vDlOffsetGap.Add(offsetGap);
      m_vDLSize.Add(size);
    }

    public string FileName
    {
      get
      {
        return m_sFileName;
      }
      set
      {
        m_sFileName = value;
      }
    }
    public void SaveFile()
    {
      File.Create(m_sFileName);

    }

    private int m_iCounter;
    private Dictionary<string, List<CFieldFormat> > m_vKeys;
    private List<int> m_vDLOffset, m_vDLSize, m_vDlOffsetGap, m_vDLSizeGap, m_vZOffset, m_vZSize, m_vZOffsetGap, m_vZSizeGap;
    private List<char> m_vZChar;
    private string m_sFileName;
  }
  class Program
  {
    static void Main(string[] args)
    {
      string path = ConfigurationManager.AppSettings["RefFile"];
      if (!File.Exists(path))
      {
        Console.WriteLine("path " + path + " does not exist");
        Console.ReadLine();
        return;
      }
      string sBasicPath = ConfigurationManager.AppSettings["ResultPath"];
      string[] cont = File.ReadAllLines(path);
      int state = 0;
      CAnalyze analyze = new CAnalyze();
      foreach (string line in cont)
      {
        if (line == "" || line == "\n" || line == "\r")
          continue;
        switch (state)
        {
          case 0:
            if (line.Length == 1 && line == "@")
              state = 1;
            break;
          case 1:
            if (line.Length == 1 && (line[0] == 28 || line[0] == 30))
              state = 2;
            break;
          case 2:
            if (!ReadAnsi(ref state, line, sBasicPath, ref analyze))
              state = 0;
            break;
          case 3:
            if (!analyze.ReadLine("", line))
            {
              state = 0;
            }
            break;
          default:
            state = 0;
            break;
        }
      }
    }

    static bool ReadAnsi(ref int state, string line, string basePath, ref CAnalyze analyze)
    {
      if (!line.StartsWith("ANSI") && !line.StartsWith("AAMVA"))
      {
        state = 0;
        return false;
      }
      int dlInd = line.IndexOf("DL");
      int dlOffset, dlSize, zOffset, zSize;
      char zChar;
      if (dlInd < 0)
      {
        state = 0;
        return false;
      }
      int offset = line.Substring(0, dlInd).Length;
      string name = basePath + line.Substring(0, dlInd) + ".txt";
      if (analyze.Counter > 0)
        analyze.SaveFile();   //saves the file and resets analyze.
      if (File.Exists(name))
      {
        analyze.ReadFile(name);
        analyze.FileName = name;
      }else
      {
        analyze.FileName = name;
      }
      analyze.Counter++;
      line = line.Substring(dlInd + 2);
      dlOffset = Convert.ToInt16(line.Substring(0, 4));
      dlSize = Convert.ToInt16(line.Substring(4, 4));
      analyze.SetDL(dlOffset, dlSize, dlOffset - offset);
      string key = line.Substring(8, 2);
      if (key == "DL")
        analyze.ReadLine("DL", line.Substring(10));
      else if (key[0] == 'Z')
      {
        zChar = key[1];
        zOffset = Convert.ToInt16(line.Substring(10, 4));
        zSize = Convert.ToInt16(line.Substring(14, 4));
        analyze.ReadLine(key, line.Substring(18));
      }
      else
        analyze.ReadLine("", line.Substring(8));
      state = 3;
      return true;
    }
  }
}
