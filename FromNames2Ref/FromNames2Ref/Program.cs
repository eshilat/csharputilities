﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;   //right click References and choose "add reference" and under "Framework" choose Configuration manager to make ConfigurationManager recognized.

namespace FromNames2Ref
{
  class Program
  {
    static void FilesCopy(string path)
    {
      string res = ConfigurationManager.AppSettings["ResFile"];
      string rf = ConfigurationManager.AppSettings["RefFile"];
      string[] vFolders;
      vFolders = Directory.GetDirectories(path);
      foreach (string folder in vFolders)
        FilesCopy(folder);
      string[] vFiles = Directory.GetFiles(path);
      string res1;
      if (vFiles.Length > 0)
      {
        res = path + "\\" + res;
        rf = path + "\\" + rf;
      }
      res1 = res;
      foreach (string fl in vFiles)
      {
        if (fl == res) //(fl == rf) 
        {
          //int ind =ConfigurationManager.AppSettings["ThePath"].Length;
          //string sub = res.Substring(ind);
          //res1 = "H:\\CSSN\\Data\\Banks\\BarCodes_Bank\\2D_Barcodes\\ErezSortedBank\\" + sub;
          try
          {
            File.Copy(res1, rf, true);
          }catch
          {
            Console.WriteLine(res1);
          }
        }
      }
    }

    static void Main(string[] args)
    {
      string thepath = ConfigurationManager.AppSettings["ThePath"]; 
      if (!Directory.Exists(thepath))
      {
        Console.WriteLine("path " + thepath + " does not exist");
        Console.ReadLine();
        return;
      }
      FilesCopy(thepath);
    }
  }
}
