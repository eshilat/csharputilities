﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace EncryptDecrypt
{
    class CWrapper
    {
        [DllImport("CImageWrapper.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        static extern void EncryptWrap(string src, string dst, bool bEncrypt);

        public void Encrypt(bool bEncrypt, string fn, string suf)
        {
            string src = fn + "." + suf;
            if (bEncrypt)
            {
                if (suf == "bmp")
                    suf = "ebmp";
                else if (suf != "ebmp")
                    suf = "ejpg";
            }
            else 
            {
                if (suf[0] != 'e')
                    return;
                if (suf == "ebmp")
                    suf = "bmp";
                else
                    suf = "jpg";
            }
            string dst = fn + "." + suf;
            EncryptWrap(src, dst, bEncrypt);
        }
    }
}
