﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;


namespace EncryptDecrypt
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            m_bEncrypt = true;
            radioEncrypt.Checked = true;
            checkSubFolders.Checked = true;
            textDlls.Text = "H:\\CSSN\\git\\CSharp\\EncryptDecrypt\\bin\\x64\\Release";
        }

        private void buttonFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fb = new FolderBrowserDialog();
            if (fb.ShowDialog() == DialogResult.OK)
            {
                textFolder.Text = fb.SelectedPath;
                textFolder.Show();
            }
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            Directory.SetCurrentDirectory(textDlls.Text);
            Cursor.Current = Cursors.WaitCursor;
            string path = textFolder.Text;
            if (!Directory.Exists(path))
            {
                MessageBox.Show(path + "does not exist");
                return;
            }
            WorkTree(checkSubFolders.Checked, m_bEncrypt, path);
            Text = "Done";
            SystemSounds.Asterisk.Play();
            Cursor.Current = Cursors.Default;

        }

        private void WorkTree(bool bSubFolders, bool bEncrypt, string path)
        {
            string[] vFiles = Directory.GetFiles(path);
            foreach (string fn in vFiles)
                ProcessFile(bEncrypt, fn);
            if (bSubFolders)
            {
                string[] vFolders = Directory.GetDirectories(path);
                foreach (string dir in vFolders)
                    WorkTree(bSubFolders, bEncrypt, dir);
            }
        }

        private void ProcessFile(bool bEncrypt, string fn)
        {
            this.Text = fn;
            int ext = fn.LastIndexOf(".");
            if (ext <= 0)
                return;
            string suf = fn.Substring(ext+1).ToLower();
            if (bEncrypt && suf != "jpg" && suf != "jpeg" && suf != "bmp" && suf != "ejpg" && suf != "ebmp")
                return;
            if (!bEncrypt && suf != "ejpg" && suf != "ebmp")
                return;
            CWrapper wrp = new CWrapper();
            wrp.Encrypt(bEncrypt, fn.Substring(0, ext), suf);
        }

        private void radioEncrypt_CheckedChanged(object sender, EventArgs e)
        {
            m_bEncrypt = true;
        }

        private void radioDecrypt_CheckedChanged(object sender, EventArgs e)
        {
            m_bEncrypt = false;
        }

        private bool m_bEncrypt;

        private void buttonDlls_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fb = new FolderBrowserDialog();
            if (fb.ShowDialog() == DialogResult.OK)
            {
                textDlls.Text = fb.SelectedPath;
                textDlls.Show();
            }
        }
    }
}
