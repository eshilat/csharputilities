#pragma once

#if defined(_MSC_VER) && defined(CIMAGEWRAPPER_EXPORTS)
#define CIMAGEWRAPPER_DECLSPEC __declspec(dllexport)
#else
#define CIMAGEWRAPPER_DECLSPEC
#endif

#ifdef __cplusplus
extern "C"
#endif
{
  extern CIMAGEWRAPPER_DECLSPEC void EncryptWrap(const char* orig, const char* dest, bool bEncrypt);
  extern CIMAGEWRAPPER_DECLSPEC void* CImageRead(const char* fn);
}
