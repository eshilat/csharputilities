
#include "pch.h"
#include "Wrapper.h"
#include <opencv2/opencv.hpp>
#include <sys/stat.h>
#include <stdio.h>
#include <wchar.h>
#ifndef _T
#ifdef UNICODE
#define _T(x) L ##x
#else
#define _T(x) x
#endif
#endif
#ifndef LPSTREAM
#define LPSTREAM void *
#endif
#include <CImage.h>
#include <opencv2/opencv.hpp>

void EncryptWrap(const char* orig, const char* dest, bool bEncrypt)
{
  CImage img;
  TCHAR sOrig[512], sDest[512];
  swprintf(sOrig, strlen(orig)+1, L"%hs", orig);
  swprintf(sDest, strlen(dest)+1, L"%hs", dest);
  img.ImportFile(sOrig);
  img.JpgCopyEncryptedFile(sOrig, bEncrypt, sDest, false);
  struct stat buffer;
  if ((stat(dest, &buffer) == 0)) //dest exists
    std::remove(orig);
}

void* CImageRead(const char* fn)
{
  CImage img;
  TCHAR sName[512];
  swprintf(sName, strlen(fn) + 1, L"%hs", fn);
  img.ImportFile(sName);
  int bc = img.GetBitCount();
  if (bc != 8 && bc != 24)
    return nullptr;
  int channels = bc / 8;
  cv::Mat* pMat = new cv::Mat(img.GetHeight(), img.GetWidth(), (bc == 8 ? CV_8UC1 : CV_8UC3));
  for (int i = 0; i < img.GetHeight(); i++)
    memcpy_s(pMat->ptr(i, 0), channels * img.GetWidth(), img.GetPtrXY(0, i), channels * img.GetWidth());
  return (void*)pMat;
}