﻿
namespace EncryptDecrypt
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textFolder = new System.Windows.Forms.TextBox();
            this.buttonFolder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.radioEncrypt = new System.Windows.Forms.RadioButton();
            this.radioDecrypt = new System.Windows.Forms.RadioButton();
            this.buttonRun = new System.Windows.Forms.Button();
            this.checkSubFolders = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textDlls = new System.Windows.Forms.TextBox();
            this.buttonDlls = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textFolder
            // 
            this.textFolder.Location = new System.Drawing.Point(159, 55);
            this.textFolder.Name = "textFolder";
            this.textFolder.Size = new System.Drawing.Size(412, 23);
            this.textFolder.TabIndex = 0;
            // 
            // buttonFolder
            // 
            this.buttonFolder.Location = new System.Drawing.Point(584, 57);
            this.buttonFolder.Name = "buttonFolder";
            this.buttonFolder.Size = new System.Drawing.Size(29, 20);
            this.buttonFolder.TabIndex = 1;
            this.buttonFolder.Text = "...";
            this.buttonFolder.UseVisualStyleBackColor = true;
            this.buttonFolder.Click += new System.EventHandler(this.buttonFolder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(110, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Folder:";
            // 
            // radioEncrypt
            // 
            this.radioEncrypt.AutoSize = true;
            this.radioEncrypt.Location = new System.Drawing.Point(670, 143);
            this.radioEncrypt.Name = "radioEncrypt";
            this.radioEncrypt.Size = new System.Drawing.Size(65, 19);
            this.radioEncrypt.TabIndex = 3;
            this.radioEncrypt.TabStop = true;
            this.radioEncrypt.Text = "Encrypt";
            this.radioEncrypt.UseVisualStyleBackColor = true;
            this.radioEncrypt.CheckedChanged += new System.EventHandler(this.radioEncrypt_CheckedChanged);
            // 
            // radioDecrypt
            // 
            this.radioDecrypt.AutoSize = true;
            this.radioDecrypt.Location = new System.Drawing.Point(670, 168);
            this.radioDecrypt.Name = "radioDecrypt";
            this.radioDecrypt.Size = new System.Drawing.Size(66, 19);
            this.radioDecrypt.TabIndex = 4;
            this.radioDecrypt.TabStop = true;
            this.radioDecrypt.Text = "Decrypt";
            this.radioDecrypt.UseVisualStyleBackColor = true;
            this.radioDecrypt.CheckedChanged += new System.EventHandler(this.radioDecrypt_CheckedChanged);
            // 
            // buttonRun
            // 
            this.buttonRun.ForeColor = System.Drawing.Color.DarkCyan;
            this.buttonRun.Location = new System.Drawing.Point(310, 245);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(125, 53);
            this.buttonRun.TabIndex = 5;
            this.buttonRun.Text = "Run";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // checkSubFolders
            // 
            this.checkSubFolders.AutoSize = true;
            this.checkSubFolders.Location = new System.Drawing.Point(159, 130);
            this.checkSubFolders.Name = "checkSubFolders";
            this.checkSubFolders.Size = new System.Drawing.Size(139, 19);
            this.checkSubFolders.TabIndex = 6;
            this.checkSubFolders.Text = "Including sub-folders";
            this.checkSubFolders.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 356);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Dll\'s folder:";
            // 
            // textDlls
            // 
            this.textDlls.Location = new System.Drawing.Point(159, 356);
            this.textDlls.Name = "textDlls";
            this.textDlls.Size = new System.Drawing.Size(412, 23);
            this.textDlls.TabIndex = 8;
            // 
            // buttonDlls
            // 
            this.buttonDlls.Location = new System.Drawing.Point(584, 356);
            this.buttonDlls.Name = "buttonDlls";
            this.buttonDlls.Size = new System.Drawing.Size(29, 23);
            this.buttonDlls.TabIndex = 9;
            this.buttonDlls.Text = "...";
            this.buttonDlls.UseVisualStyleBackColor = true;
            this.buttonDlls.Click += new System.EventHandler(this.buttonDlls_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonDlls);
            this.Controls.Add(this.textDlls);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkSubFolders);
            this.Controls.Add(this.buttonRun);
            this.Controls.Add(this.radioDecrypt);
            this.Controls.Add(this.radioEncrypt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonFolder);
            this.Controls.Add(this.textFolder);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textFolder;
        private System.Windows.Forms.Button buttonFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioEncrypt;
        private System.Windows.Forms.RadioButton radioDecrypt;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.CheckBox checkSubFolders;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textDlls;
        private System.Windows.Forms.Button buttonDlls;
    }
}

