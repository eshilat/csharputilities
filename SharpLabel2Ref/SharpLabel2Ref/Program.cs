﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace SharpLabel2Ref
{
    class Program
    {
        static void Main(string[] args)
        {
            string srefF = ConfigurationManager.AppSettings["ref"];
            string sLabel = ConfigurationManager.AppSettings["labels"];
            int iBlurs = Int32.Parse(ConfigurationManager.AppSettings["blurs"]);
            int ind = sLabel.LastIndexOf("\\");
            string path, outF;
            if (ind < 0)
                path = ".\\";
            else
                path = sLabel.Substring(0, ind + 1);
            outF = path + srefF;

            string[] vLabels = File.ReadAllLines(sLabel);
            List<string> vRef = new List<string>();
            foreach (string s in vLabels)
            {
                int col = s.IndexOf(':');
                if (col < 0)
                    continue;
                Console.WriteLine(s);
                int iFirstBlur = Int32.Parse(s.Substring(col + 2));
                if (iFirstBlur > 0)
                    vRef.Add(s.Substring(0, col + 1) + " 1");
                else
                    vRef.Add(s.Substring(0, col + 1) + " 0");
                for (int i = 1; i <= iBlurs; i++)
                {
                    string fn = s.Substring(0, col - 5) + "_"+i.ToString() + ".jpg";
                    if (iFirstBlur > i)
                        vRef.Add(fn + " : 1");
                    else
                        vRef.Add(fn + " : 0");
                }
            }
            File.WriteAllLines(outF, vRef.ToArray());
            Console.WriteLine("Done");
        }
    }
}
