﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;   //right click References and choose "add reference" and under "Framework" choose Configuration manager to make ConfigurationManager recognized.

namespace Ref2RocBase
{
    class Program
    {
        static void Main(string[] args)
        {
            string refF = ConfigurationManager.AppSettings["ref"];
            string sApp = ConfigurationManager.AppSettings["app"];
            string sType = ConfigurationManager.AppSettings["type"];
            int ind = refF.LastIndexOf("\\");
            string resF, path, outF;
            if (ind < 0)
                path = ".\\";
            else
                path = refF.Substring(0, ind+1);
            resF = path + ConfigurationManager.AppSettings["res"];
            outF = path + ConfigurationManager.AppSettings["output"];

            StreamWriter roc = File.CreateText(outF);
            string[] vRef = File.ReadAllLines(refF);
            string[] vRes = File.ReadAllLines(resF);
            if (vRef.Length != vRes.Length)
            {
                Console.WriteLine("Ref and Res files do not match");
                return;
            }
            for (int i = 0; i < vRef.Length; i++)
            {
                string sref = vRef[i], sres = vRes[i];
                if (sType == "onlyWhite")
                {
                    int indref = sref.LastIndexOf("_White"), indres = sres.LastIndexOf("_White");
                    if (indref < 0 || indres < 0)
                        continue;
                }
                if (sApp == "sharpness")
                {
                    int indSemiColRef = sref.LastIndexOf(":");
                    int indSemiColRes = sres.LastIndexOf(":");
                    if (indSemiColRef < 0 || indSemiColRes < 0)
                    {
                        Console.WriteLine("Wrong file structure");
                        return;
                    }
                    string fnRef = sref.Substring(0, indSemiColRef-1);
                    string fnRes = sres.Substring(0, indSemiColRes);
                    int slsh = fnRef.LastIndexOf('\\');
                    if (slsh >= 0)
                        fnRef = fnRef.Substring(slsh + 1);
                    if (fnRef != fnRes)
                    {
                        Console.WriteLine("files don't match");
                        return;
                    }
                    sref = sref.Substring(indSemiColRef + 2);
                    int refVal = Int32.Parse(sref);
                    sres = sres.Substring(indSemiColRes + 2);
                    int endVal = sres.IndexOf("left");
                    float resVal = float.Parse(sres.Substring(12, endVal-13));
                    roc.WriteLine(fnRef + ", " + refVal.ToString() + ", " + resVal.ToString());
                }else if (sApp == "glare")
                {
                    int indSemiColRef = sref.LastIndexOf(":");
                    int indSemiColRes = sres.LastIndexOf(":");
                    if (indSemiColRef < 0 || indSemiColRes < 0)
                    {
                        Console.WriteLine("Wrong file structure");
                        return;
                    }
                    string fnRef = sref.Substring(0, indSemiColRef);
                    string fnRes = sres.Substring(0, indSemiColRes);
                    int slsh = fnRef.LastIndexOf('\\');
                    if (fnRef != fnRes)
                    {
                        Console.WriteLine("files don't match");
                        return;
                    }
                    if (slsh >= 0)
                        fnRef = fnRef.Substring(slsh + 1);
                    sref = sref.Substring(sref.Length - 2);
                    float refVal = float.Parse(sref);
                    sres = sres.Substring(sres.Length - 2);
                    float resVal = float.Parse(sres.Substring(sres.Length - 2));
                    roc.WriteLine(fnRef + ", " + refVal.ToString() + ", " + resVal.ToString());
                }
                else
                {
                    int dotsRef = sref.LastIndexOf("="), dotsRes = sres.LastIndexOf("=");
                    if (dotsRef < 0 || dotsRes < 0)
                    {
                        Console.WriteLine("Ref and Res files do not match 3");
                        return;
                    }
                    string sLabel = sref.Substring(dotsRef + 1), sGrade = sres.Substring(dotsRes + 1);
                    float fLabel = float.Parse(sLabel);
                    float fGrade = float.Parse(sGrade);
                    if (fLabel == 0)
                        roc.WriteLine("FALSE, " + fGrade.ToString());
                    else if (fLabel == 1)
                        roc.WriteLine("TRUE, " + fGrade.ToString());
                }
            }
            roc.Close();
        }
    }
}
