﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing;



namespace RefFileCreate
{
  class Program
  {
    private const int iProgMode = 2;  //1 - triples, 2 - list of files, 3 - Eric's png croped files
    //??? Note: Only one of the iProgMode-dependent branches is reachable since the variable is const. Ignore.
    static void Main(string[] args)
    {
      Console.WriteLine("Please enter the path");
      string path = Console.ReadLine();
      if (!Directory.Exists(path))
      {
        Console.WriteLine("path " + path + " does not exist");
        Console.ReadLine();
        return;
      }
      string[] vFolders;
      if (iProgMode == 1 || iProgMode == 3)
        vFolders = Directory.GetDirectories(path);
      else
       vFolders = Directory.GetFiles(path);
      string fn;
      if (iProgMode != 3)
      {
        Console.WriteLine("Please write the name of the reference file");
        if (path[path.Length - 1] != '\\')
          path += '\\';
        fn = path + Console.ReadLine();
      }
      for (int i = 0; i < vFolders.Length; i++)
      {
        if (iProgMode == 2)
        {
          int ind = vFolders[i].LastIndexOf('.');
          string sufix = vFolders[i].Substring(ind + 1);
          if (sufix == "ejpg" || sufix == "ebmp" || sufix == "jpg" || sufix == "jpeg" || sufix == "bmp")
            vFolders[i] = /*"Triple: " +*/ vFolders[i].Substring(path.Length, vFolders[i].Length - path.Length);
          else
            vFolders[i] = " ";
        }
        else if (iProgMode == 1)
          vFolders[i] = "Triple: " + vFolders[i].Substring(path.Length, vFolders[i].Length - path.Length);
        else if (iProgMode == 3)
        {
          int ind = vFolders[i].LastIndexOf('\\');
          string name = vFolders[i].Substring(ind + 1);
          File.Copy(vFolders[i] + "\\white.png", path + "\\" + name + ".png");
        }
        else
          Console.WriteLine("Unsupported program mode");
      }
      if (iProgMode != 3)
        File.WriteAllLines(fn, vFolders);
      Console.WriteLine("Done");
      Console.ReadLine();
    }
  }
}
