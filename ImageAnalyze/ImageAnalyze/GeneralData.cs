﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;
using System.Configuration;
using System.Windows.Forms;


namespace ImageAnalyze
{
  struct GeneralData
  {
    public string imageName;
    public string AcuantDllsPath;
  }

  class ImageMeasurementsWrap
  {
    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    static extern bool SetDllDirectory(string lpPathName);

    [DllImport("ImageMeasurements.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
    static extern float ImageMeasurementsSharpnessComputeByName(StringBuilder imageName, ref float fLeftRightBalance, ref float topBottomBalance);

    [DllImport("ImageMeasurements.dll", CallingConvention = CallingConvention.Cdecl)]
    static extern float ImageMeasurementsSharpnessCompute(IntPtr inImg, ref float fLeftRightBalance, ref float fTopBotBalance);

    [DllImport("ImageMeasurements.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
    static extern float ImageMeasurementsGlareGradeByName(StringBuilder imageName);

    [DllImport("ImageMeasurements.dll", CallingConvention = CallingConvention.Cdecl)]
    static extern void ImageMeasurementsSetResultsImage();

    [DllImport("ImageMeasurements.dll", CallingConvention = CallingConvention.Cdecl)]
    static extern IntPtr ImageMeasurementsGetResultsImage();


    public ImageMeasurementsWrap()
    {
      SetDllDirectory(ConfigurationManager.AppSettings["AcuantDllsPath"]);
    }

    public float IMGlareComputeByName(StringBuilder imageName)
    {
      try
      {
        return ImageMeasurementsGlareGradeByName(imageName);
      }catch(Exception ex)
      {
        MessageBox.Show("ImageMeasurementsGlareGradeByName Failed. Error " + ex.ToString() );
        return 1;
      }
    }
    public float IMSharpnessComputeByName(StringBuilder imageName, ref float fLeftRightBalance, ref float topBottomBalance)
    {
      try
      {
        return ImageMeasurementsSharpnessComputeByName(imageName, ref fLeftRightBalance, ref topBottomBalance);
      }
      catch (Exception ex)
      {
        MessageBox.Show("Failed to call ImageMeasurementsSharpnessComputeByName. Error " + ex.ToString());
        return 0;
      }
    }

    public float IMSharpnessCompute(IntPtr inImg, ref float fLeftRightBalance, ref float fTopBotBalance)
    {
      try
      {
        fLeftRightBalance = 0;
        return ImageMeasurementsSharpnessCompute(inImg, ref fLeftRightBalance, ref fTopBotBalance);
      }
      catch (Exception ex)
      {
        MessageBox.Show("Failed to call ImageMeasurementsSharpnessCompute. error " + ex.ToString());
        return 0;
      }
    }
  }
}
