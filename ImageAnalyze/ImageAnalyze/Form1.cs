﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Configuration; //for ConfigurationManager to be recognized had to right-click References
                            //and to choose System Configurations there.

namespace ImageAnalyze
{
  public partial class Form1 : Form
  {
    private GeneralData m_general;
    private ImageMeasurementsWrap m_IMWrap;
    private bool m_bBatch, m_bEncodedImg;
    private delegate void SingleImageOp();
    private string[] m_vsBatchResults;
    private int m_iBatchResultIndex;
    private PictureBox m_pb;

    public Form1()
    {
      InitializeComponent();
      m_general = new GeneralData();
      m_general.AcuantDllsPath = ConfigurationManager.AppSettings["AcuantDllsPath"];
      m_IMWrap = new ImageMeasurementsWrap();
      m_bBatch = false;
      m_bEncodedImg = false;
    }

    private void GlareGradeImage()
    {
      float fGlare = 1;
      StringBuilder sb = new StringBuilder();
      sb.Append(m_general.imageName);
      fGlare = m_IMWrap.IMGlareComputeByName(sb);
      if (m_bBatch)
      {
        int ind = m_general.imageName.LastIndexOf('\\');
        string fn = m_general.imageName.Substring(ind + 1);
        string output = fn + ": Glare = " + fGlare.ToString();
        m_vsBatchResults[m_iBatchResultIndex] = output;
        m_iBatchResultIndex++;
      }
      else
        MessageBox.Show("Glare = " + fGlare.ToString() );
    }

    private void FocusGradeImage()
    {
      float fSharpness = 0, fLeftRight = 0, fTopBot = 0;
      if (m_bEncodedImg)
      {
        StringBuilder sb = new StringBuilder();
        sb.Append(m_general.imageName);
        fSharpness = m_IMWrap.IMSharpnessComputeByName(sb, ref fLeftRight, ref fTopBot);
      }
      else
      {
        try
        {
          Bitmap img = (Bitmap)Image.FromFile(m_general.imageName);
          IntPtr inImg = img.GetHbitmap();
          m_bEncodedImg = false;
          fSharpness = m_IMWrap.IMSharpnessCompute(inImg, ref fLeftRight, ref fTopBot);
          img.Dispose();
        }
        catch (System.IO.FileNotFoundException)
        {
          MessageBox.Show("File not found");
        }
      }
      if (m_bBatch)
      {
        int ind = m_general.imageName.LastIndexOf('\\');
        string fn = m_general.imageName.Substring(ind+1);
        string output = fn + ": Sharpness = " + fSharpness.ToString() + " left//right balance = " + fLeftRight.ToString() + " top//bottom balance = " + fTopBot.ToString();
        m_vsBatchResults[m_iBatchResultIndex] = output;
        m_iBatchResultIndex++;
      }
      else
        MessageBox.Show("Sharpness = " + fSharpness.ToString() + " left//right balance = " + fLeftRight.ToString() + " top//bottom balance = " + fTopBot.ToString());
    }

    private void BatchWork(string batchFileName, SingleImageOp op )
    {
      if (!m_bBatch)
        return;
      string[] list = File.ReadAllLines(batchFileName);
      string orig = batchFileName;
      m_vsBatchResults = new string[list.Length];
      int ind = batchFileName.LastIndexOf('\\');
      string path;
      if (ind <= 0)
        path = ".\\";
      else
        path = batchFileName.Substring(0, ind+1);
      m_iBatchResultIndex = 0;
      foreach (string fn in list)
      {
        ind = fn.LastIndexOf('.');
        string sufix = fn.Substring(ind + 1);
        //if (sufix == "ejpg" || sufix == "ebmp")
        //  m_bEncodedImg = true;
        //else
        //  m_bEncodedImg = false;
        m_bEncodedImg = (sufix != "png");
        m_general.imageName = path + fn;
        this.Text = fn;
        op();
      }
      m_general.imageName = orig;
      this.Text = "DONE!";
      File.WriteAllLines(path + ConfigurationManager.AppSettings["BatchResultFile"], m_vsBatchResults, Encoding.Unicode);
    }

    private void focusGradeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (m_bBatch)
      {
        BatchWork(m_general.imageName, FocusGradeImage);
      }
      else
      {
        FocusGradeImage();
      }
    }

    private void focusToolStripMenuItem_Click(object sender, EventArgs e) //the menue item changed to operations
    {
    }

    private void glareGradeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (m_bBatch)
      {
        BatchWork(m_general.imageName, GlareGradeImage);
      }
      else
      {
        GlareGradeImage();
      }
    }

    private void Form1_ResizeEnd(object sender, EventArgs e)
    {
      m_pb.Size = new Size(this.Size.Width-50, this.Size.Height-50);
    }

    private void openToolStripMenuItem_Click(object sender, EventArgs e)
    {
      OpenFileDialog ofd = new OpenFileDialog();
      ofd.InitialDirectory = ConfigurationManager.AppSettings["InitialPath"];
      ofd.Filter = "batch files (.txt)|*.txt|ejpg files (.ejpg)|*.ejpg|ebmp files (.ebmp)|.ebmp|jpg files (.jpg)|*.jpg|bmp files (.bmp)|*.bmp|png files (*.png)|*.png";
      ofd.RestoreDirectory = true;
      ofd.FilterIndex = 1;
      if (ofd.ShowDialog() == DialogResult.OK)
      {
        m_general.imageName = ofd.FileName;
        if (!File.Exists(m_general.imageName))
        {
          MessageBox.Show(m_general.imageName + " does not exist");
          return;
        }
        int ind = m_general.imageName.LastIndexOf('.');
        string sufix = m_general.imageName.Substring(ind + 1);
        if (sufix == "txt")
        {
          m_bBatch = true;
        }
        else
        {
          m_bBatch = false;
          if (sufix == "ejpg" || sufix == "ebmp")
          {
            m_bEncodedImg = true;
          }
          else
          {
            m_bEncodedImg = false;
            m_pb = new PictureBox();
            m_pb.Image = Image.FromFile(m_general.imageName);
            m_pb.Location = new Point(0, 0);
            m_pb.SizeMode = PictureBoxSizeMode.StretchImage;
            m_pb.Size = new Size(this.Size.Width-50, this.Size.Height-50);
            this.Controls.Add(m_pb);
          }
        }
      }
    }
  }
}
