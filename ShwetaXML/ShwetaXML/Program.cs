﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;   //right click References and choose "add reference" and under "Framework" choose Configuration manager to make ConfigurationManager recognized.

namespace ShwetaXML
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = ConfigurationManager.AppSettings["path"];
            int ind = path.LastIndexOf('\\');
            string basePath = "";
            if (ind > 0)
                basePath = path.Substring(0, ind);
            StreamWriter glareListFile = File.CreateText(basePath + "\\glareList.txt");
            StreamWriter sharpListFile = File.CreateText(basePath + "\\sharpList.txt");
            StreamWriter glareRef = File.CreateText(basePath + "\\glareRef.txt");
            StreamWriter sharpRef = File.CreateText(basePath + "\\sharpRef.txt");
            string[] vLines = File.ReadAllLines(path);
            int cnt = 0;
            foreach (string sLine in vLines)
            {
                if (++cnt < 3)
                    continue;
                int fileInd = sLine.IndexOf('\"');
                if (fileInd < 0)
                    break;
                string guid = sLine.Substring(fileInd + 1, 36);
                int typeInd = sLine.IndexOf("type=\""), typeEndInd = sLine.Substring(typeInd+6).IndexOf('\"');
                string type = sLine.Substring(typeInd+6, typeEndInd);
                int valInd = sLine.IndexOf("<value>"), valEndInd = sLine.Substring(valInd + 7).IndexOf('<');
                int val = Convert.ToInt32(sLine.Substring(valInd + 7, valEndInd) );
                if (type == "ImageGlare")
                {
                    glareListFile.WriteLine(guid);
                    glareRef.WriteLine(guid + ".jpg " + val.ToString());
                }
                else
                {
                    sharpListFile.WriteLine(guid);
                    sharpRef.WriteLine(guid + ".jpg " + val.ToString());
                }
            }
            glareRef.Close();
            glareListFile.Close();
            sharpRef.Close();
            sharpListFile.Close();
        }
    }
}
