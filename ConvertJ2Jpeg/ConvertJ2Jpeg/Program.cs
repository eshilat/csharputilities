﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConvertJ2Jpeg
{
  class Program
  {
    const int START_INDX = 1;
    static void Main(string[] args)
    {
      Console.WriteLine("Please enter the path");
      string path = Console.ReadLine();
      int indx = START_INDX;
      if (!Directory.Exists(path))
      {
        Console.WriteLine("path " + path + " does not exist");
        Console.ReadLine();
        return;
      }
      string[] vFolders;
      vFolders = Directory.GetDirectories(path);
      for (int i = 0; i < vFolders.Length; i++)
      {
        int lc;
        lc = vFolders[i].LastIndexOf('\\');
        string[] vFiles = Directory.GetFiles(vFolders[i]);
        string cur = vFolders[i].Substring(lc + 1);
        for (int n = 0; n < vFiles.Length; n++)
        {
          int ind = vFiles[n].LastIndexOf('.');
          string name, sufix = vFiles[n].Substring(ind + 1).ToLower();
          int ind2 = vFiles[n].LastIndexOf('\\');
          string baseName = vFiles[n].Substring(ind2 + 1, ind - ind2);
          name = path + '\\' + indx.ToString("D5") + "_" + cur + "_" + baseName + "jpg";
          if (baseName.ToLower() != "white." && baseName.ToLower() != "nearinfrared." && baseName.ToLower() != "back-white.")
            continue;
          indx++;
          Console.WriteLine(vFiles[n]);
          //if (sufix == "jpeg" || sufix == "jpg")
          //  File.Copy(vFiles[n], name);
          //else
          {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C \"C:\\Program Files (x86)\\IrfanView\\i_view32.exe\" " + vFiles[n] + " /convert=" + name;
            process.StartInfo = startInfo;
            process.Start();
          }
        }
      }
    }
  }
}
