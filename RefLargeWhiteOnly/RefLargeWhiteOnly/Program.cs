﻿using System;
using System.IO;
using System.Collections.Generic;

namespace RefLargeWhiteOnly
{
    public class CFileTree
    {
        public CFileTree(string path, string fnOut)
        {
            m_sPath = path;
            m_sFileOut = fnOut;
            m_vList = new List<string>();
        }

        public void Create(string path)
        {
            string[] vFolders;
            string[] vFiles;
            vFolders = Directory.GetDirectories(path);
            vFiles = Directory.GetFiles(path);
            foreach (string file in vFiles)
            {
                int ind = file.LastIndexOf('.');
                if (ind <= 0)
                    continue;
                string fn = file.Substring(0, ind);
                char ch = fn[fn.Length - 1];
 //               if (ch >= '0' && ch <= '9')
                {
                    string local = file.Substring(m_sPath.Length);
                    m_vList.Add(local);
                }
            }
            foreach (string folder in vFolders)
                Create(folder);
        }

        public void FillFile()
        {
            int sz = m_vList.Count;
            string[] all = new string[sz];
            for (int i = 0; i < sz; i++)
                all[i] = m_vList[i];
            File.WriteAllLines(m_sFileOut, all);
        }

        private string m_sPath, m_sFileOut, m_sOrig;
        private List<string> m_vList;
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the path");
            string path = Console.ReadLine();
            if (!Directory.Exists(path))
            {
                Console.WriteLine("path " + path + " does not exist");
                Console.ReadLine();
                return;
            }
            Console.WriteLine("Please write the name of the reference file");
            if (path[path.Length - 1] != '\\')
                path += '\\';
            string fn = path + Console.ReadLine();
            CFileTree ft = new CFileTree(path, fn);
            ft.Create(path);
            ft.FillFile();
        }
    }
}
