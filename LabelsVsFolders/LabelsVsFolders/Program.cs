﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;   //right click References and choose "add reference" and under "Framework" choose Configuration manager to make ConfigurationManager recognized.


namespace LabelsVsFolders
{
  class Program
  {
    static void Main(string[] args)
    {
      string sbasePath = ConfigurationManager.AppSettings["basicPath"];
      string sref = ConfigurationManager.AppSettings["ref"];
      string sfolder0 = ConfigurationManager.AppSettings["folder0"];
      string sfolder100 = ConfigurationManager.AppSettings["folder100"];

      StreamWriter glareMessFile = File.CreateText(sbasePath + "\\glareMess.txt");

      string[] vFiles0 = Directory.GetDirectories(sbasePath + sfolder0);
      string[] vFiles100 = Directory.GetDirectories(sbasePath + sfolder100);

      string[] vLines = File.ReadAllLines(sbasePath + sref);
      foreach (string sline in vLines)
      {
        int ind = sline.IndexOf(".jpg");
        if (ind < 0)
          continue;
        string snum = sline.Substring(ind + 5);
        string scur = sline.Substring(0, ind);
        int inum = Convert.ToInt32(snum);
        bool bFound = false;
        if (inum == 0)
        {
          foreach (string sfile in vFiles0)
            if ((ind = sfile.IndexOf(scur)) >= 0)
            {
              bFound = true;
              break;
            }
          if (!bFound)
            glareMessFile.Write(sline);
        }
        else
        {
          foreach (string sfile in vFiles100)
            if ((ind = sfile.IndexOf(scur)) >= 0)
            {
              bFound = true;
              break;
            }
          if (!bFound)
            glareMessFile.Write(sline);

        }
      }
    }
  }
}
