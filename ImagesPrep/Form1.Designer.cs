﻿namespace ImagesPrep
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Start = new System.Windows.Forms.Button();
            this.borderPortion = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.folderSelect = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textFolder = new System.Windows.Forms.TextBox();
            this.Stat = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.borderPortion)).BeginInit();
            this.SuspendLayout();
            // 
            // Start
            // 
            this.Start.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Start.ForeColor = System.Drawing.Color.Coral;
            this.Start.Location = new System.Drawing.Point(311, 330);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(185, 76);
            this.Start.TabIndex = 0;
            this.Start.Text = "Run";
            this.Start.UseVisualStyleBackColor = false;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // borderPortion
            // 
            this.borderPortion.DecimalPlaces = 2;
            this.borderPortion.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.borderPortion.Location = new System.Drawing.Point(160, 36);
            this.borderPortion.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.borderPortion.Name = "borderPortion";
            this.borderPortion.Size = new System.Drawing.Size(120, 22);
            this.borderPortion.TabIndex = 1;
            this.borderPortion.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Frame portion";
            // 
            // folderSelect
            // 
            this.folderSelect.Location = new System.Drawing.Point(721, 78);
            this.folderSelect.Name = "folderSelect";
            this.folderSelect.Size = new System.Drawing.Size(29, 22);
            this.folderSelect.TabIndex = 3;
            this.folderSelect.Text = "...";
            this.folderSelect.UseVisualStyleBackColor = true;
            this.folderSelect.Click += new System.EventHandler(this.folderSelect_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Read from:";
            // 
            // textFolder
            // 
            this.textFolder.Location = new System.Drawing.Point(160, 78);
            this.textFolder.Name = "textFolder";
            this.textFolder.Size = new System.Drawing.Size(542, 22);
            this.textFolder.TabIndex = 5;
            // 
            // Stat
            // 
            this.Stat.Location = new System.Drawing.Point(567, 331);
            this.Stat.Name = "Stat";
            this.Stat.Size = new System.Drawing.Size(134, 74);
            this.Stat.TabIndex = 6;
            this.Stat.Text = "Statistics";
            this.Stat.UseVisualStyleBackColor = true;
            this.Stat.Click += new System.EventHandler(this.Stat_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Stat);
            this.Controls.Add(this.textFolder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.folderSelect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.borderPortion);
            this.Controls.Add(this.Start);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.borderPortion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.NumericUpDown borderPortion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button folderSelect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textFolder;
        private System.Windows.Forms.Button Stat;
    }
}

