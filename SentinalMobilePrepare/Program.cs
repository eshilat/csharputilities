﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SentinalMobilePrepare
{
    struct SSensor
    {
        public List<string> vSensrs;
        public int numOfMobiles;
        public int numOfCards;
    }

    struct STypes
    {
        public List<string> vTypes;
        public int numOfCards;
    }

    class CSMPrep
    {
        private int m_iLen;


        private string[] String2Fields(string line)
        {
            List<string> vFields = new List<string>();
            int ind2 = line.IndexOf(",,");
            for (; ind2 >= 0; ind2 = line.IndexOf(",,"))
                line = line.Substring(0, ind2) + ", None," + line.Substring(ind2 + 2);
            for (int i = 0; line.Length > 0; i++)
            {
                int ind = line.IndexOf(",");
                if (ind < 0)
                    if (i < m_iLen - 1 && m_iLen > 0)
                        for (int j = i + 1; j < m_iLen; j++)
                            vFields.Add("None");
                    else if (m_iLen == 0)
                        m_iLen = i + 1;
                if (ind > 0)
                {
                    vFields.Add(line.Substring(0, ind).Trim());
                    line = line.Substring(ind + 1);
                }
                else
                {
                    vFields.Add(line.Trim());
                    line = "";
                }
            }
            if (m_iLen == 0)
                m_iLen = vFields.Count;

            return vFields.ToArray();
        }

        private int IsValid(string dev, ref string sDev)
        {
            string dv = dev.ToLower();
            sDev = string.Empty;
            bool smrt = dv.IndexOf("combosmart") >= 0, prmc = dv.IndexOf("prmc") >= 0, scn = dv.IndexOf("comboscan") >= 0;
            if (dv == "id-120" || dv == "id-150" || dv == "id-1501" || dv == "at9000mk2" || smrt || dv == "penta" || prmc || scn )
            {
                sDev = dv;
                if (smrt)
                    sDev = "combosmart";
                if (prmc)
                    sDev = "prmc";
                if (scn)
                    sDev = "comboscan";
                return 1;
            }
            else if (dv.IndexOf("mobile") >= 0)
            {
                sDev = "mobile";
                return 2;
            }
            return 0;
        }

        public void Run(string[] csv, string sentinal, string mobile)
        {
            string srmv = "/data/CVML_Team/PhotoTampering/";
            string sbase = "H:\\CSSN\\Data\\Banks\\ML_Data\\PhotoTamperingSet1\\";
            Dictionary<string, SSensor> vTypes = new Dictionary<string, SSensor>();
            Dictionary<string, STypes> vSensor = new Dictionary<string, STypes>();
            List<string> vMobile = new List<string>(), vSent = new List<string>();
            List<string> vCopyFails = new List<string>();
            if (!Directory.Exists(sentinal))
                Directory.CreateDirectory(sentinal);
            if (!Directory.Exists(mobile))
                Directory.CreateDirectory(mobile);
            for (int fl = 0; fl < 3; fl++)
            {
                string[] vIn = File.ReadAllLines(csv[fl]);
                for (int i = 1; i < vIn.Length; i++)
                {
                    string[] vFields = String2Fields(vIn[i]);
                    string sDev = string.Empty;
                    int iValid = IsValid(vFields[13], ref sDev);
                    if (iValid == 0)
                        continue;
                    string dest;
                    if (iValid == 1)
                        dest = sentinal;
                    else
                        dest = mobile;
                    string sloc = string.Empty;
                    if (vFields[1].IndexOf(srmv) < 0)
                    {
                        int ind = vFields[1].LastIndexOf("/");
                        if (ind >= 0)
                            sloc = vFields[1].Substring(ind + 1) + "\\" + vFields[2].Substring(0, vFields[2].Length - 3) + "ejpg";
                        else
                            continue;
                    }else
                        sloc = vFields[1].Substring(srmv.Length) + "\\" + vFields[2].Substring(0, vFields[2].Length - 3) + "ejpg";

                    try
                    {
                        File.Copy(sbase + sloc, dest + vFields[4] + ".ejpg");
                    }
                    catch (UnauthorizedAccessException e)
                    {
                        vCopyFails.Add(e.ToString() + ", " + sbase + sloc + "," + dest + vFields[4] + ".ejpg");
                        continue;
                    }
                    catch (ArgumentException e)
                    {
                        vCopyFails.Add(e.ToString() + ", " + sbase + sloc + "," + dest + vFields[4] + ".ejpg");
                        continue;
                    }
                    catch (PathTooLongException e)
                    {
                        vCopyFails.Add(e.ToString() + ", " + sbase + sloc + "," + dest + vFields[4] + ".ejpg");
                        continue;
                    }
                    catch (DirectoryNotFoundException e)
                    {
                        vCopyFails.Add(e.ToString() + ", " + sbase + sloc + "," + dest + vFields[4] + ".ejpg");
                        continue;
                    }
                    catch (FileNotFoundException e)
                    {
                        vCopyFails.Add(e.ToString() + ", " + sbase + sloc + "," + " Not relevant");
                        continue;
                    }
                    catch (IOException e)
                    {
                        if (!File.Exists(dest + vFields[4] + ".ejpg"))
                            vCopyFails.Add(e.ToString() + ", " + sbase + sloc + "," + dest + vFields[4] + ".ejpg");
                        else
                            vCopyFails.Add("Repeated, " + vFields[4]);
                        continue;   //destination exists
                    }
                    catch (NotSupportedException e)
                    {
                        vCopyFails.Add(e.ToString() + ", " + sbase + sloc + "," + " Not relevant");
                        continue;
                    }
                    catch (Exception e)
                    {
                        if (!File.Exists(dest + vFields[4] + ".ejpg"))
                            vCopyFails.Add(e.ToString() + ", " + sbase + sloc + "," + dest + vFields[4] + ".ejpg");
                        continue;   //destination exists
                    }

                    try
                    {
                        STypes typ = new STypes();
                        if (vSensor.TryGetValue(sDev, out typ))
                        {
                            typ.numOfCards++;
                            if (!typ.vTypes.Contains(vFields[6]))
                                typ.vTypes.Add(vFields[6]);
                            vSensor[sDev] = typ;
                        }
                        else
                        {
                            typ.vTypes = new List<string>();
                            typ.vTypes.Add(vFields[6]);
                            typ.numOfCards = 1;
                            vSensor.Add(sDev, typ);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    try
                    {
                        SSensor sens = new SSensor();
                        if (vTypes.TryGetValue(vFields[6], out sens))
                        {
                            sens.numOfCards++;
                            if (iValid == 2)
                                sens.numOfMobiles++;
                            if (!sens.vSensrs.Contains(sDev))
                                sens.vSensrs.Add(sDev);
                            vTypes[vFields[6]] = sens;
                        }
                        else
                        {
                            sens.vSensrs = new List<string>();
                            sens.vSensrs.Add(sDev);
                            sens.numOfCards = 1;
                            sens.numOfMobiles = 0;
                            if (iValid == 2)
                                sens.numOfMobiles = 1;
                            vTypes.Add(vFields[6], sens);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    if (dest == mobile)
                    {
                        vMobile.Add(sloc + "," + vFields[13] + "," + vFields[5] + "," + vFields[6]);
                    }
                    else if (dest != sentinal)
                        vCopyFails.Add("Bad destination, " + sbase + sloc + "," + dest + vFields[4] + ".ejpg");
                    else
                    {
                        vSent.Add(sloc + "," + vFields[13] + "," + vFields[5] + "," + vFields[6]);
                    }

                }

            }
            File.WriteAllLines(mobile + "mobile.csv", vMobile);
            File.WriteAllLines(sentinal + "sentinal.csv", vSent);
            List<string> vSensorList = new List<string>();
            vSensorList.Add("Device, Num of cards, Num of types");
            foreach (KeyValuePair<string, STypes> pair in vSensor.ToList())
            {
                STypes typ = pair.Value;
                vSensorList.Add(pair.Key + ", " + typ.numOfCards.ToString() + ", " + typ.vTypes.Count.ToString());
            }
            List<string> vTypesList = new List<string>();
            vTypesList.Add("Cards type, Num of cards, Num of mobiles, Num of sensors, Status");
            foreach(KeyValuePair<string, SSensor> pair in vTypes.ToList())
            {
                SSensor sens = pair.Value;
                string stat = "Both";
                if (sens.numOfCards == sens.numOfMobiles)
                    stat = "Mobile only";
                if (sens.numOfMobiles == 0)
                    stat = "Sentinal only";
                vTypesList.Add(pair.Key + ", " + sens.numOfCards.ToString() + ", " + sens.numOfMobiles.ToString() + ", " + sens.vSensrs.Count + ", " + stat);
            }
            File.WriteAllLines(sbase + "sensors.csv", vSensorList.ToArray());
            File.WriteAllLines(sbase + "types.csv", vTypesList.ToArray());
            File.WriteAllLines(sbase + "problems.csv", vCopyFails.ToArray());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string[] csv = { "H:\\CSSN\\Data\\Banks\\ML_Data\\PhotoTamperingSet1\\Valid_final_phase4_v2.csv", "H:\\CSSN\\Data\\Banks\\ML_Data\\PhotoTamperingSet1\\Test_final_phase4_v2.csv", "H:\\CSSN\\Data\\Banks\\ML_Data\\PhotoTamperingSet1\\Train_final_phase4_v2.csv" };
            string sentinal = "H:\\CSSN\\Data\\Banks\\ML_Data\\PhotoTamperingSet1\\Sentinal\\";
            string mobile = "H:\\CSSN\\Data\\Banks\\ML_Data\\PhotoTamperingSet1\\Mobile\\";
            CSMPrep prp = new CSMPrep();
            prp.Run(csv, sentinal, mobile);
        }

    }
}
