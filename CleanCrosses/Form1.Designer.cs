﻿
namespace CleanCrosses
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textExcelToClean = new System.Windows.Forms.TextBox();
            this.buttonExcelToClean = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonClean = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textExcelToClean
            // 
            this.textExcelToClean.Location = new System.Drawing.Point(130, 65);
            this.textExcelToClean.Name = "textExcelToClean";
            this.textExcelToClean.Size = new System.Drawing.Size(517, 20);
            this.textExcelToClean.TabIndex = 0;
            // 
            // buttonExcelToClean
            // 
            this.buttonExcelToClean.Location = new System.Drawing.Point(654, 65);
            this.buttonExcelToClean.Name = "buttonExcelToClean";
            this.buttonExcelToClean.Size = new System.Drawing.Size(31, 19);
            this.buttonExcelToClean.TabIndex = 1;
            this.buttonExcelToClean.Text = "...";
            this.buttonExcelToClean.UseVisualStyleBackColor = true;
            this.buttonExcelToClean.Click += new System.EventHandler(this.buttonExcelToClean_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Excel to clean:";
            // 
            // buttonClean
            // 
            this.buttonClean.Location = new System.Drawing.Point(333, 209);
            this.buttonClean.Name = "buttonClean";
            this.buttonClean.Size = new System.Drawing.Size(157, 61);
            this.buttonClean.TabIndex = 3;
            this.buttonClean.Text = "Clean";
            this.buttonClean.UseVisualStyleBackColor = true;
            this.buttonClean.Click += new System.EventHandler(this.buttonClean_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonClean);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonExcelToClean);
            this.Controls.Add(this.textExcelToClean);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textExcelToClean;
        private System.Windows.Forms.Button buttonExcelToClean;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonClean;
    }
}

