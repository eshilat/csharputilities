﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CleanCrosses
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            textExcelToClean.Text = string.Empty;
        }

        private void buttonExcelToClean_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.InitialDirectory = "H:\\CSSN\\Data\\Banks\\ML_Data\\PhotoTamperingSet1\\";
            fd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            fd.FilterIndex = 2;
            fd.RestoreDirectory = true;

            if (fd.ShowDialog() == DialogResult.OK)
            {
                //Get the path of specified file
                string filePath = fd.FileName;

                //Read the contents of the file into a stream
                textExcelToClean.Text = filePath;
            }
        }

        private void buttonClean_Click(object sender, EventArgs e)
        {
            if (textExcelToClean.Text == string.Empty)
                return;
            int dot = textExcelToClean.Text.LastIndexOf(".");
            string baseFn = textExcelToClean.Text.Substring(0, dot);
            string extFn = textExcelToClean.Text.Substring(dot + 1);
            string[] vLines = File.ReadAllLines(textExcelToClean.Text);
            string[] vRes = new string[vLines.Length];
            int iRes = 1;
            vRes[0] = vLines[0];
            for (int i = 1; i < vLines.Length; i++)
            {
                int ind = vLines[i].LastIndexOf(".");
                int coma = vLines[i].LastIndexOf(",");
                if (ind < 0 || coma < 0 || coma > ind)
                    continue;
                int len = vLines[i].Length;
                vRes[iRes] = vLines[i].Substring(0, Math.Min(ind + 3, len));
                iRes++;
                Text = i.ToString();
            }
            File.WriteAllLines(baseFn+"Fix."+extFn, vRes);
            Text = "Done";
        }
    }
}
