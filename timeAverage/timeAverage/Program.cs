﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace timeAverage
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "h:\\results\\times.txt";
            string[] times = File.ReadAllLines(path);
            float min = 10000000, max = 0, avg = 0;
            int cnt = 0, mnLoc = 0, mxLoc = 0, above100 = 0;
            foreach (string line in times)
            {
                int ind = line.LastIndexOf("Total:");
                if (ind < 0)
                    continue;
                cnt++;
                string end = line.Substring(ind + 7);
                float val = (float)Convert.ToDouble(end);
                if (val > 100)
                {
                    Console.WriteLine(val.ToString() + " at "+cnt.ToString());
                    above100++;
                }
                if (val > max)
                {
                    max = val;
                    mxLoc = cnt;
                }
                if (val < min)
                {
                    min = val;
                    mnLoc = cnt;
                }
                avg += val;
            }
            avg /= cnt;
            Console.WriteLine("range from " + min.ToString() + " (" + mnLoc.ToString() + ") to " + max.ToString() + " ("+ mxLoc.ToString() +"). avg: " + avg.ToString()+". There are "+above100.ToString()+" above 100.");
        }
    }
}
