﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/*
 * Receives a file of SMS and a folder containing the actual SMS folders downloaded using that file.
 * The software replaces the files list by the list of missing folders of the original list.
 * The user is expected to move the folders before another call to ExportDataFromSms
 */

namespace SMSListUpdate
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Please enter the SMS list file full path");
      string smsFile = Console.ReadLine();
      if (!File.Exists(smsFile))
      {
        Console.WriteLine("File " + smsFile + " does not exist");
        Console.ReadLine();
        return;
      }
      Console.WriteLine("SMS samples folder: ");
      string smsPath = Console.ReadLine();
      if (!Directory.Exists(smsPath))
      {
        Console.WriteLine("Folder " + smsPath + "does not exist");
        Console.ReadLine();
        return;
      }
      string[] vCandidates;
      vCandidates = File.ReadAllLines(smsFile);
      string[] vFolders;
      List<string> vNew = new List<string>();
      vFolders = Directory.GetDirectories(smsPath);
      Console.WriteLine("Sorting candidates and folder lists...");
      Array.Sort<string>(vCandidates);
      Array.Sort<string>(vFolders);
      Console.WriteLine("Start working");
      int i = 0, j = 0;
      while (i < vCandidates.Length && j < vFolders.Length)
      {
        string sFldr;
        int ind = vFolders[j].LastIndexOf('\\');
        if (ind < 0)
          sFldr = vFolders[j];
        else
          sFldr = vFolders[j].Substring(ind + 1);
        for (; i < vCandidates.Length && String.Compare(vCandidates[i], sFldr) < 0; i++)
        {
          vNew.Add(vCandidates[i]);
//          Console.WriteLine(vCandidates[i]);
        }
        if (i >= vCandidates.Length)
          break;
        int cmp = 0;
        for (; i < vCandidates.Length && j < vFolders.Length; i++, j++)
        {
          ind = vFolders[j].LastIndexOf('\\');
          if (ind < 0)
            sFldr = vFolders[j];
          else
            sFldr = vFolders[j].Substring(ind + 1);
          cmp = string.Compare(vCandidates[i], sFldr);
          //if (cmp > 0)
          //{
          //  Console.WriteLine("Error: list - " + vCandidates[i] + " folder - " + sFldr +".Press any key to continue");
          //  Console.ReadLine();
          //  return;
          //}
          if (cmp < 0)
            break;
        }

      }
      for (; i < vCandidates.Length; i++)
        vNew.Add(vCandidates[i]);
      if (vNew.Count() > 0)
      {
        File.WriteAllLines(smsFile, vNew.ToArray());
        Console.WriteLine("Ready for next round");
      }
      else
      {
        Console.WriteLine("Done");
        Console.ReadLine();
        File.Delete(smsFile);
      }
    }
  }
}
