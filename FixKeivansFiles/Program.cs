﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace FixKeivansFiles
{
    class CArrange
    {
        private int m_iLen;

        public CArrange()
        {
            m_iLen = 0;
        }

        private string[] String2Fields(string line)
        {
            List<string> vFields = new List<string>();
            int ind2 = line.IndexOf(",,");
            for (; ind2 >= 0; ind2 = line.IndexOf(",,"))
                line = line.Substring(0, ind2) + ", None," + line.Substring(ind2 + 2);
            for (int i = 0; line.Length > 0; i++)
            {
                int ind = line.IndexOf(",");
                if (ind < 0)
                    if (i < m_iLen - 1 && m_iLen > 0)
                    {
                        for (int j = i + 1; j < m_iLen; j++)
                            vFields.Add("None");
                        i = m_iLen - 1;
                    }
                    else if (m_iLen == 0)
                        m_iLen = i + 1;
                if (ind > 0)
                {
                    vFields.Add(line.Substring(0, ind).Trim());
                    line = line.Substring(ind + 1);
                }
                else
                {
                    vFields.Add(line.Trim());
                    line = "";
                }
            }
            if (m_iLen == 0)
                m_iLen = vFields.Count;

            return vFields.ToArray();
        }

        public void Arrange()
        {
            List<string> vKeivan = new List<string>();
            vKeivan.Add("H:/CSSN/Data/Banks/ML_Data/PhotoTamperingSet1/Valid_final_phase4_v2.csv");
            const int FLDR_NUM = 7;
            List<string[]> vBanks = new List<string[]>();
            for (int i = 0; i < 3; i++)
                vBanks.Add(new string[1]);
            vBanks.Add(File.ReadAllLines("H:/CSSN/Data/Banks/ML_Data/PhotoTamperingSet1/updated_10-13-21.csv"));
            vBanks.Add(File.ReadAllLines("H:/CSSN/Data/Banks/ML_Data/PhotoTamperingSet1/updated_10-7-21.csv"));
            vBanks.Add(File.ReadAllLines("H:/CSSN/Data/Banks/ML_Data/PhotoTamperingSet1/updated_10-8-21.csv"));
            vBanks.Add(File.ReadAllLines("H:/CSSN/Data/Banks/ML_Data/PhotoTamperingSet1/Top50MobileTypes/updatedTop50.csv"));
            foreach (string skvn in vKeivan)
            {
                string[] vLines = File.ReadAllLines(skvn);
                for (int ln = 1; ln < vLines.Length; ln++)
                {
                    Console.WriteLine(ln.ToString() + " out of " + vLines.Length.ToString()); 
                    string line = vLines[ln];
                    string[] vFields = String2Fields(line);
                    int[] c = new int[FLDR_NUM];
                    c[0] = vFields[1].IndexOf("SupportedTypes_5-14-21");
                    c[1] = vFields[1].IndexOf("UnsortedForManualTamper");
                    c[2] = vFields[1].IndexOf("UnsupportedTypes_5-7-21");
                    c[3] = vFields[1].IndexOf("Unsupported_Types_10-13-21");
                    c[4] = vFields[1].IndexOf("UnsupportedTypes_10-7-21");
                    c[5] = vFields[1].IndexOf("UnsupportedTypes_10-8-21");
                    c[6] = vFields[1].IndexOf("Top50MobileTypes");
                    int ind = -1;
                    for (int i = 0; i < FLDR_NUM; i++)
                        if (c[i] >= 0)
                        {
                            ind = i;
                            break;
                        }
                    if (ind < 0)
                    {
                        int a = 1;
                    }
                    bool bFound = false;
                    if (vFields[2] == "None")
                    {
                        bFound = true;
                        string sguid = vFields[4];
                        string scur;
                        foreach (string sline in vBanks[ind])
                        {
                            int iguid = sline.IndexOf(sguid);
                            if (iguid >= 0)
                            {
                                int cm1 = sline.IndexOf(',');
                                if (cm1 < 0)
                                {
                                    int a = 1;
                                }
                                scur = sline.Substring(cm1 + 1);
                                int cm2 = scur.IndexOf(',');
                                if (cm2 < 0)
                                {
                                    int a = 1;
                                }
                                string simg = scur.Substring(0, cm2);
                                vFields[2] = simg;
                                break;
                            }
                        }
                    }else if (vFields[28].ToLower().IndexOf(vFields[2].ToLower()) < 0)
                    {
                        Console.WriteLine("line: " + ln.ToString() + "Note: " + vFields[2] + " vs. "+ vFields[26] + "\n");
                    }
                    if (vFields[17] == "None")
                    {
                        bFound = true;
                        int a = 1;
                    }
                    if (bFound)
                    {
                        string newline = "";
                        for (int i = 0; i < vFields.Length - 1; i++)
                            newline += (vFields[i] + ",");
                        newline += vFields[vFields.Length - 1];
                        vLines[ln] = newline;
                    }
                }
                File.WriteAllLines(skvn, vLines);
            }
            Console.WriteLine("DONE!");
        }
    }


    class Program
    {

        static void Main(string[] args)
        {
            CArrange arr = new CArrange();
            arr.Arrange();
        }
    }
}
