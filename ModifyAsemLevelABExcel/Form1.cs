﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ModifyAsemLevelABExcel
{
    public partial class Form1 : Form
    {
        private string m_sSupported, m_sUnsupported, m_sTest, m_sRes;
        private int m_iLen;
        public Form1()
        {
            InitializeComponent();
            m_sSupported = "H:\\CSSN\\Data\\Banks\\ML_Data\\SupportedTypes_5-14-21\\updatedSupported.csv";
            m_sUnsupported = "H:\\CSSN\\Data\\Banks\\ML_Data\\UnsupportedTypes_5-7-21\\updatedUnsupported_2.csv";
            m_sTest = "H:\\CSSN\\Data\\Banks\\ML_Data\\AutoFakeReport20210713.csv";
            m_sRes = "H:\\CSSN\\Data\\Banks\\ML_Data\\AutoFakeReport20210713_fixed.csv";
            m_iLen = 0;
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            string[] vSupported = File.ReadAllLines(m_sSupported);
            string[] vUnsupported = File.ReadAllLines(m_sUnsupported);
            string[] vTest = File.ReadAllLines(m_sTest);
            string[] vOut = new string[vTest.Length];
            vOut[0] = vTest[0];

            for (int i = 1; i < vTest.Length; i++)
            {
                m_iLen = 0;
                string[] vFields = String2Fields(vTest[i]);
                for (int j = 0; j <= 20; j++)
                    vOut[i] += (vFields[j] + ",");
                string fn = vFields[3];
                bool bSupported = false;
                int ind = fn.IndexOf("Unsupported");
                if (ind < 0)
                {
                    ind = fn.IndexOf("Supported");
                    bSupported = true;
                }
                else
                {
                    int lastSlash = fn.LastIndexOf("\\");
                    fn = fn.Substring(0, lastSlash - 1);
                }
                fn = fn.Substring(ind);
                ind = fn.IndexOf("\\");
                fn = fn.Substring(ind + 1);
                if (bSupported)
                { 
                    ind = fn.IndexOf(".");
                    fn = fn.Substring(0, ind);
                }
                Text = fn;

                string line = string.Empty;
                if (bSupported)
                    foreach (string ln in vSupported)
                    {
                        int det = ln.IndexOf(fn);
                        if (det >= 0)
                        {
                            line = ln;
                            break;
                        }
                    }
                else
                    foreach (string ln in vUnsupported)
                    {
                        int det = ln.IndexOf(fn);
                        if (det >= 0)
                        {
                            line = ln;
                            break;
                        }
                    }
                if (line == string.Empty)
                {
                    MessageBox.Show("Can't find " + fn);
                    return;
                }
                m_iLen = 0;
                string[] vAddFields = String2Fields(line);
                vOut[i] += (vAddFields[17] + ",");
                float fsclx = 0, fscly = 0;
                fsclx = (float)Int32.Parse(vFields[17]) / Int32.Parse(vFields[19]);
                fscly = (float)Int32.Parse(vFields[18]) / Int32.Parse(vFields[20]);
                vOut[i] += (fsclx.ToString() + "," + fscly.ToString());
            }
            File.WriteAllLines(m_sRes, vOut);
            Text = "Done";
        }

        private string[] String2Fields(string line)
        {
            List<string> vFields = new List<string>();
            int ind2 = line.IndexOf(",,");
            for (; ind2 >= 0; ind2 = line.IndexOf(",,"))
                line = line.Substring(0, ind2) + ", None," + line.Substring(ind2 + 2);
            for (int i = 0; line.Length > 0; i++)
            {
                int ind = line.IndexOf(",");
                if (ind < 0)
                    if (i < m_iLen - 1 && m_iLen > 0)
                        return null;
                    else if (m_iLen == 0)
                        m_iLen = i + 1;
                if (ind > 0)
                {
                    vFields.Add(line.Substring(0, ind).Trim());
                    line = line.Substring(ind + 1);
                }
                else
                {
                    vFields.Add(line.Trim());
                    line = "";
                }
            }
            if (m_iLen == 0)
                m_iLen = vFields.Count;

            return vFields.ToArray();
        }

    }
}
