﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;   //right click References and choose "add reference" and under "Framework" choose Configuration manager to make ConfigurationManager recognized.

namespace GlennFile4Sharp
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = ConfigurationManager.AppSettings["path"];
            string refFile = path + ConfigurationManager.AppSettings["refFile"];
            string resFileN = path + ConfigurationManager.AppSettings["resFileNew"];
            string resFileD = path + ConfigurationManager.AppSettings["resFileDev"];
            StreamWriter roc = File.CreateText(path + "rocBase.csv");
            StreamWriter rocOld = File.CreateText(path + "rocBaseOld.csv");
            StreamWriter bm = File.CreateText(path + "bigMistakes.txt");
            string[] vRef = File.ReadAllLines(refFile);
            string[] vResN = File.ReadAllLines(resFileN);
            string[] vResD = File.ReadAllLines(resFileD);

            int sz1 = vResN.Length, sz2 = vResD.Length;
            if (sz1 != sz2)
            {
                Console.WriteLine("Sizes of res file are different");
                return;
            }
            float fThreshold = float.Parse(ConfigurationManager.AppSettings["Threshold"]);
            for (int i = 0; i < sz1; i++)
            {
                string sLineN = vResN[i];
                string sLineD = vResD[i];
                string sLineR = vRef[i];
                int ind = sLineN.IndexOf("_White-1");
                if (ind < 0)
                    break;
                string sGuid = sLineN.Substring(0, ind);
                int dots = sLineR.IndexOf(":");
                string sLabel = sLineR.Substring(dots+1, sLineR.Length - dots - 1);
                int iLabel = int.Parse(sLabel);
                int zn = sLineN.IndexOf("0."), zd = sLineD.IndexOf("0.");
                int ln = sLineN.IndexOf("left"), ld = sLineD.IndexOf("left");
                string sFltn = sLineN.Substring(zn, ln - zn);
                string sFltd = sLineD.Substring(zd, ld - zd);
                float graden = float.Parse(sFltn), graded = float.Parse(sFltd);
                float dif;
                if ((dif = Math.Abs(graden - graded)) > fThreshold)
                    bm.WriteLine(sGuid + " " + dif.ToString());
                if (iLabel == 0)
                {
                    roc.WriteLine("FALSE, " + sFltn);
                    rocOld.WriteLine("FALSE, " + sFltd);
                }
                else
                {
                    roc.WriteLine("TRUE, " + sFltn);
                    rocOld.WriteLine("TRUE, " + sFltd);
                }
                //for (int j = 0; j < vRef.Length; j++)
                //    if ((ind = vRef[j].IndexOf(sGuid)) >= 0)
                //    {
                //        if ((ind = vRef[j].Substring(41).IndexOf("1")) >= 0)
                //            roc.WriteLine("TRUE, " + sFltn);
                //        else
                //            roc.WriteLine("FALSE, " + sFltn);
                //        break;
                //    }
            }
            bm.Close();
            roc.Close();
        }
    }
}
